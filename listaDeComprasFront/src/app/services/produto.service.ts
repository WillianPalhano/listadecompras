import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Produto } from '../models/produto.model';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  constructor(
    private http: HttpClient,
  ) {
  }

  getProduto(): Observable<Produto[]> {
    return this.http.get<Produto[]>(
      "http://localhost:8080/produtos",
    )
  }

  setProduto(produto): Observable<Produto> {
    return this.http.post<Produto>(
      "http://localhost:8080/produtos",
      produto
    )
  }

  updateProduto(produto): Observable<Produto> {
    return this.http.put<Produto>(
      "http://localhost:8080/produtos",
      produto
    )
  }
}
