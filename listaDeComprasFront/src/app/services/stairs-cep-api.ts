import { Http, RequestOptions, URLSearchParams, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
// import 'rxjs/add/operator/map';

@Injectable()
export class StairsCepAPI {
  url: string =  'https://cep.stairs.rest';
  constructor(public http: Http) {}

  get(endpoint: string, params?: any, options?: RequestOptions) {
    let headers = new Headers();
    headers.append('Key','stairscreativestudiorocks');
    headers.append('Signature','d752b3b6ad67e6d804ddca0ae1ce31a4f1997428647f4505e777ff3abd65dcd5fdb86034a037f6917c823868b2ae35dcf0054671e8961de61800477094ac2ff1' );

    if (!options) {
      options = new RequestOptions({headers: headers});
    }

    // Support easy query params for GET requests
    if (params) {
      let p = new URLSearchParams();
      for (let k in params) {
        p.set(k, params[k]);
      }
      // Set the search field if we have params and don't already have
      // a search field set in options.
      options.search = !options.search && p || options.search;
    }
    return this.http.get(this.url + '/' + endpoint, options);
  }

//   getCep(numero: string) {
//     const url = 'cep/' + numero
//     return this.get(url).toPromise().then(
//         ()=>{
//             res => res.json()[0]
//         }
//     )
//   }
  post(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.post(this.url + '/' + endpoint, body, options);
  }

  put(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  delete(endpoint: string, options?: RequestOptions) {
    return this.http.delete(this.url + '/' + endpoint, options);
  }

  patch(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }
}
