import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Solicitacao } from '../models/solicitacao.model';

@Injectable({
  providedIn: 'root'
})
export class SolicitacaoService {
  SOLICITACAO: BehaviorSubject<Solicitacao[]> = new BehaviorSubject(undefined)
  VALOR_TOTAL: BehaviorSubject<number> = new BehaviorSubject(undefined)

  constructor(
    private http: HttpClient,
  ) {
  }

  getSolicitacao(): Observable<Solicitacao[]> {
    return this.http.get<Solicitacao[]>(
      "http://localhost:8080/solicitacoes",
    )
  }

  setSolicitacao(solicitacao): Observable<Solicitacao> {
    return this.http.post<Solicitacao>(
      "http://localhost:8080/solicitacoes",
      solicitacao
    )}
}
