import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: HttpClient,
  ) {
  }

  getUsuario(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(
      "http://localhost:8080/usuarios",
    )
  }

  setUsuario(usuario): Observable<Usuario> {
    return this.http.post<Usuario>(
      "http://localhost:8080/usuarios",
      usuario
    )}
}
