import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProdutosComponent } from './produtos.component';
import { RouterModule } from '@angular/router';

const PRODUTOS_ROUTE =[
  {path: '', component: ProdutosComponent}
];

@NgModule({
  declarations: [ProdutosComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(PRODUTOS_ROUTE)
  ]
})
export class ProdutosModule { }
