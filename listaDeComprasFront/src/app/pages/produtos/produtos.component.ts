import { Component, OnInit } from '@angular/core';
import { Produto } from '../../models/produto.model';
import { Solicitacao } from 'src/app/models/solicitacao.model';
import { SolicitacaoService } from 'src/app/services/solicitacao.service';
import { ProdutoService } from 'src/app/services/produto.service';


@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.scss']
})
export class ProdutosComponent implements OnInit {

  constructor(
    private solicitacaoService: SolicitacaoService,
    private produtoService: ProdutoService
    ) { }

  solicitacoes: Solicitacao[] = []
  produtos: Produto[] = []
  produtosExibidos: Produto[] = []
  valorTotal: number = 0
  
  ngOnInit() {
    this.produtoService.getProduto().toPromise()
    .then((res)=>{
      this.produtos = res
      this.produtosExibidos = res
      console.log('Produtos resgatados com sucesso:', res);
    })
    .catch((err)=>{
      console.log('Erro ao pegar produtos:', err);
    })
  }

  solicitarProduto(i: number) {
    var dateFormat = require('dateformat');
    var now = new Date();
    dateFormat(now, "isoDateTime");

    if (this.produtosExibidos[i].quantidade > 0) {
      let solicitacao: Solicitacao = new Solicitacao
      let flag = false
      for (let j = 0; j < this.solicitacoes.length; j++) {
        if (this.solicitacoes[j].produto == this.produtosExibidos[i].nome) {
          this.solicitacoes[j].quantidade++
          this.solicitacoes[j].valorTotal = this.produtosExibidos[i].preco * this.solicitacoes[j].quantidade
          this.valorTotal += this.solicitacoes[j].valorTotal
          console.log('VALOR TOTAL:', this.valorTotal);
          
          flag = true
        }
      }

      if (flag == false) {
        solicitacao.dataSolicitacao = dateFormat(now, "dd/mm/yyyy").toString()
        solicitacao.produto = this.produtosExibidos[i].nome
        solicitacao.quantidade = 1
        solicitacao.usuario = localStorage.getItem('usuario')
        solicitacao.valorTotal = this.produtosExibidos[i].preco * solicitacao.quantidade
        this.valorTotal += solicitacao.valorTotal

        this.solicitacoes.push(solicitacao)
      }
      console.log('Solicitado:', this.solicitacoes);
      this.produtosExibidos[i].quantidade--
    } else {
      this.openModal('Ops!', 'Infelizmente, não temos mais este produto em estoque :(')
    }
  }

  exibirProduto(i: number) {
    this.openModal(undefined, undefined, this.produtosExibidos[i].linkImg)
  }

  solicitar(){
    if(this.solicitacoes.length != 0){
      this.solicitacoes.forEach(solicitacao => {
        this.solicitacaoService.setSolicitacao(solicitacao).toPromise()
        .then((res)=>{
          console.log('Solicitação armazenada:', res);
          this.openModal('Concluído!', 'Recebemos sua solicitação :D')
          console.log('VALOR TOTAL:', this.valorTotal);
          
          this.solicitacaoService.VALOR_TOTAL.next(this.valorTotal)
        }).catch((err)=>{
          console.log('Erro ao salvar solicitação:', err);
        })
        for (let i = 0; i < this.produtosExibidos.length; i++) {
          if (solicitacao.produto == this.produtosExibidos[i].nome) {
            this.produtoService.updateProduto(this.produtosExibidos[i]).toPromise()
            .then((res)=>{
              console.log('Produto Atualizado:', res);
            })
            .catch((err)=>{
              console.log('Erro ao atualizar produto:', err);
            })
          }
        }
      });
      
      this.solicitacaoService.SOLICITACAO.next(this.solicitacoes)
      this.solicitacoes = []
      this.produtos = this.produtosExibidos
    }else{
      this.openModal('Ops!', 'Por favor, selecione algum produto.')
    }
    
  }

  cancelarSolicitacao(){
    if(this.solicitacoes.length != 0){
      this.solicitacoes = []
      this.produtoService.getProduto().toPromise()
      .then((res)=>{
        this.produtosExibidos = res
        console.log('Produtos resgatados com sucesso:', res);
      })
      .catch((err)=>{
        console.log('Erro ao pegar produtos:', err);
      })
      this.openModal('', 'Solicitação cancelada.')
      // this.produtosExibidos = this.produtos
    }else{
      this.openModal('Ops!', 'Nenhum produto foi selecionado.')
    }
  }

  //INICIO - Configuração da modal
  tituloModal: string
  msgModal: string
  botaoFechar: string
  modal: number = 0
  imgModal: string = ''

  openModal(titulo: string, msg: string, imgModal?: string) {
    this.tituloModal = titulo
    this.msgModal = msg
    this.botaoFechar = 'Fechar'
    this.imgModal = imgModal
    this.modal++
  }

  closeModal() {
    if (this.modal > 0) {
      this.modal--
    }
  }
  //FIM - Configuração da modal

}
