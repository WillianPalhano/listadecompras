import { Component, OnInit } from '@angular/core';
import { Solicitacao } from 'src/app/models/solicitacao.model';
import { SolicitacaoService } from 'src/app/services/solicitacao.service';

@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.component.html',
  styleUrls: ['./relatorios.component.scss']
})
export class RelatoriosComponent implements OnInit {

  solicitacoes: Solicitacao[] = []
  valorTotal: number = 0.00
  constructor(
    private solicitacaoService: SolicitacaoService
  ) {
    this.solicitacaoService.SOLICITACAO.subscribe(solicitacoes => {
      this.solicitacoes = solicitacoes;
    })
    this.solicitacaoService.VALOR_TOTAL.subscribe(valorTotal => {
      this.valorTotal = valorTotal
    })
   }

  ngOnInit() {
    this.buscarSolicitacoes()
  }

  buscarSolicitacoes(){
    this.solicitacaoService.getSolicitacao()
    .toPromise()
    .then((res)=>{
      // this.solicitacoes = res
      // console.log('Solicitacao ARRAY:', this.solicitacoes);
      this.solicitacaoService.SOLICITACAO.next(res)
      console.log('Solicitacao:', this.solicitacoes);
      let somaTotal: number
      for (let i = 0; i < this.solicitacoes.length; i++) {
        somaTotal += this.solicitacoes[i].valorTotal
        console.log('VALOR TOTAL:',somaTotal);
      }
      this.solicitacaoService.VALOR_TOTAL.next(somaTotal)

    }).catch((err)=>{
      console.log('Erro ao pegar solicitações:', err);
    })
  }

  // somarValorTotal(){
  //   setTimeout(async ()=>{
  //     let somaTotal
  //     for (let i = 0; i < this.solicitacoes.length; i++) {
  //       somaTotal += this.solicitacoes[i].valorTotal
  //       console.log('VALOR TOTAL:',somaTotal);
  //     }
  //     await this.solicitacaoService.VALOR_TOTAL.next(somaTotal)
  //   }, 1000)
  // }
}
