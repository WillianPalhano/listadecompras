import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroComponent } from './cadastro.component';
import { RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';

const CADASTRO_ROUTE =[
  {path: '', component: CadastroComponent}
];

@NgModule({
  declarations: [CadastroComponent],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild(CADASTRO_ROUTE)
  ]
})
export class CadastroModule { }
