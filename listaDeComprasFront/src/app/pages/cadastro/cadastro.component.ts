import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../models/usuario.model';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import { StairsCepAPI } from 'src/app/services/stairs-cep-api';
import { ESTADOS, ESTADOS_SIGLA } from 'src/app/mocks/estados.mock';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {

  constructor(
    private router: Router,
    private usuarioService: UsuarioService,
    private stairsCepApi: StairsCepAPI
  ) { }

  estados: string[] = ESTADOS
  estadosSigla: string[] = ESTADOS_SIGLA
  cadastro: Usuario = new Usuario()
  confirmaSenha: string

  ngOnInit() {
  }

  //INICIO - Configuração da modal
  tituloModal: string
  msgModal: string
  botaoFechar: string
  modal: number = 0

  openModal(titulo: string, msg: string) {
    this.tituloModal = titulo
    this.msgModal = msg
    this.botaoFechar = 'Fechar'
    this.modal++
  }

  closeModal() {
    if (this.modal > 0) {
      this.modal--
    }
  }
  //FIM - Configuração da modal

  cadastrarUsuario(form) {

    if (!form.valid) {
      this.openModal('Ops!', 'Parece que alguns campos não estão preenchidos corretamente. Por favor, revise seu cadastro.')
    } else {
      if (this.cadastro.senha == this.confirmaSenha) {
        console.log('Senhas são iguais');
        console.log('Usuário cadastrado:', form.value);
        console.log('Oq tem nas variaveu:', this.cadastro);
        //Cadastrar usuario
        this.usuarioService.setUsuario(form.value).toPromise()
          .then((res) => {
            console.log('Usuario armazenada:', res);
          }).catch((err) => {
            console.log('Erro ao salvar usuario:', err);
          })
        this.openModal('Concluído!', 'Usuário cadastrado com sucesso! Você será redirecionado para o site...')
        localStorage.setItem('usuario', this.cadastro.login)

        setTimeout(() => {
          this.router.navigateByUrl(`/main`)
        }, 2500)
      } else {
        this.openModal('Ops!', 'As senhas inseridas não conferem.')
      }
    }
  }
  
  changeCEP(event) {
    let enderecoCEP
    let cepSoNumero;
    cepSoNumero = event.target.value
    cepSoNumero = cepSoNumero.replace(/[^a-zA-Z0-9]/g, '');
    console.log('valorCEP:', cepSoNumero);

    if (cepSoNumero.length == 8) {
      this.stairsCepApi.get('cep/' + cepSoNumero).toPromise()
        .then((res) => {
          console.log('vem isso aqui:', res.json());

          if (res.json() != "") {
            enderecoCEP = res.json()[0];
            this.cadastro.logradouro = enderecoCEP['tipo_logradouro'] + ' ' + enderecoCEP['logradouro']
            this.cadastro.UF = this.obterEstado(enderecoCEP['uf'])
            console.log('UF:', this.cadastro.UF);

            this.cadastro.cidade = enderecoCEP['municipio']
            console.log('CEP:', enderecoCEP);
          } else {
            this.openModal('', 'CEP não encontrado, favor inserir os dados de endereço manualmente.')
          }
        }).catch((err) => {
          this.openModal('', 'CEP não encontrado, favor inserir os dados de endereço manualmente.')
          console.log('Erro ao consultar CEP:', err);
        });
    }
  }

  obterEstado(termoPesquisar: string) {
    //item.toLocaleLowerCase().includes(this.termoPesquisar.toLocaleLowerCase()) ? item : undefined      
    let index = this.estadosSigla.indexOf(termoPesquisar.toLocaleUpperCase())
    return index ? this.estadosSigla[index] : ''
  }
}