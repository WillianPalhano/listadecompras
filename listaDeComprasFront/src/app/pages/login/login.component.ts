import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login: string
  senha: string



  constructor(
    private usuarioService: UsuarioService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  logarUsuario(form) {
    console.log(this.login, this.senha);
    if (!this.login || !this.senha) {
      this.openModal('Ops!', 'Alguns campos não foram preenchidos.')
    } else {
      //Faz Login
      let flag = false
      this.usuarioService.getUsuario().toPromise()
        .then((res) => {
          for (let i = 0; i < res.length; i++) {
            if (this.login == res[i].login) {
              if (this.senha == res[i].senha) {
                localStorage.setItem('usuario', this.login)
                this.router.navigateByUrl(`/main`)
                flag = true;
                break;
              }
            }
          }
          if (flag == false) {
            this.openModal('Ops!', 'Usuário ou senha incorretos.')
          }
        })
    }
  }

  //INICIO - Configuração da modal
  tituloModal: string
  msgModal: string
  botaoFechar: string
  modal: number = 0

  openModal(titulo: string, msg: string) {
    this.tituloModal = titulo
    this.msgModal = msg
    this.botaoFechar = 'Fechar'
    this.modal++
  }

  closeModal() {
    if (this.modal > 0) {
      this.modal--
    }
  }
  //FIM - Configuração da modal

}
