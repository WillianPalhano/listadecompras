import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { RouterModule } from '@angular/router';
import { ProdutosComponent } from '../produtos/produtos.component';
import { RelatoriosComponent } from '../relatorios/relatorios.component';

const MAIN_ROUTE =[
  {path: '', component: MainComponent}
];

@NgModule({
  declarations: [MainComponent, ProdutosComponent, RelatoriosComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(MAIN_ROUTE)
  ]
})
export class MainModule { }
