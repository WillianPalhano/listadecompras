import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SolicitacaoService } from 'src/app/services/solicitacao.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(
    private router: Router,
    private solicitacaoService: SolicitacaoService
    ) { }

  relatorio: boolean = false
  produtos: boolean = true

  ngOnInit() {
    
  }

  abrirProdutos(){
    console.log('Abrindo Produtos...');
    this.relatorio = false
    setTimeout(()=>{
      this.produtos = true
    }, 250)
  }

  abrirRelatorios(){
    this.solicitacaoService.getSolicitacao()
    .toPromise()
    .then((res)=>{
      this.solicitacaoService.SOLICITACAO.next(res)
      let valorTotal: number
    }).catch((err)=>{
      console.log('Erro ao pegar solicitações:', err);
    })

    console.log('Abrindo Relatorios...');
    this.produtos = false
    setTimeout(()=>{
      this.relatorio = true
    }, 250)
  }


  sair(){
    localStorage.setItem('usuario', '')
    this.router.navigate(['/login']);
    console.log('Saiu');
  }

}
