export class Produto {
    id: number
    nome: string
    descricao: string
    linkImg: string
    medida: string
    preco: number
    quantidade: number
}