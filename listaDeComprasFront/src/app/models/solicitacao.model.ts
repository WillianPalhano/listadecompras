export class Solicitacao {
    id: number
    usuario: string
    dataSolicitacao: string = ''
    produto: string
    quantidade: number
    valorTotal: number
}
