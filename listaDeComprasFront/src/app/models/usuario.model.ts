export class Usuario {
    login: string
    senha: string
    nome: string
    dataNascimento: string
    idade : string
    sexo: string
    CEP: any
    logradouro?: string = ''
    numero?: string
    complemento?: string
    cidade?: string
    UF?: string   
}