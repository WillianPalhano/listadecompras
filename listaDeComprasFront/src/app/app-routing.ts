import { Routes, RouterModule } from '@angular/router';

const ROUTES: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', loadChildren: './pages/login/login.module#LoginModule' },
    { path: 'cadastro', loadChildren: './pages/cadastro/cadastro.module#CadastroModule' },
    { path: 'main', loadChildren: './pages/main/main.module#MainModule' },
    // { path: '**', redirectTo: 'login'}
    // { path: '**', redirectTo: 'checkout'}
];

export const routing = RouterModule.forRoot(ROUTES);