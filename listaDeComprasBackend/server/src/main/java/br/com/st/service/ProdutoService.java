package br.com.st.service;

import java.util.List;

import br.com.st.domain.Produto;

public interface ProdutoService {

    List<Produto> findAll();
    Produto create(Produto u);
    Produto save(Produto u);
}
