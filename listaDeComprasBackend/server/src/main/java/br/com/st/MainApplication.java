package br.com.st;

import br.com.st.domain.Produto;
import br.com.st.domain.Usuario;
import br.com.st.repository.ProdutoRepository;
import br.com.st.repository.UsuarioRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.filter.CorsFilter;

import java.util.Collections;
import java.util.Date;


@SpringBootApplication
public class MainApplication {

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    @Bean
    ApplicationRunner init(ProdutoRepository produtoRepository) {
        return args -> {

           Produto p1 = new Produto();
           p1.setNome("Game of Thrones");
           p1.setDescricao("Autor: George R. R. Martin");
           p1.setLinkImg("../../../assets/box-gameofthrones.jpg");
           p1.setMedida("Box com 5 livros");
           p1.setPreco(200.00);
           p1.setQuantidade(10);
           produtoRepository.save(p1);

           Produto p2 = new Produto();
           p2.setNome("Crime e Castigo");
           p2.setDescricao("Autor: Fiódor Dostoiévski");
           p2.setLinkImg("./../assets/crime-e-castigo.jpg");
           p2.setMedida("1 livro");
           p2.setPreco(40.00);
           p2.setQuantidade(15);
           produtoRepository.save(p2);

           Produto p3 = new Produto();
           p3.setNome("Harry Potter e a Pedra Filosofal");
           p3.setDescricao("Autora: J.K. Rowling");
           p3.setLinkImg("./../assets/hp-1.jpg");
           p3.setMedida("1 livro");
           p3.setPreco(45.50);
           p3.setQuantidade(5);
           produtoRepository.save(p3);

           Produto p4 = new Produto();
           p4.setNome("Harry Potter e o Cálice de Fogo");
           p4.setDescricao("Autora: J.K. Rowling");
           p4.setLinkImg("./../assets/hp-4.jpg");
           p4.setMedida("1 livro");
           p4.setPreco(30.00);
           p4.setQuantidade(8);
           produtoRepository.save(p4);

           Produto p5 = new Produto();
           p5.setNome("Harry Potter e as Relíquias da Morte");
           p5.setDescricao("Autora: J.K. Rowling");
           p5.setLinkImg("./../assets/hp-reliquias.jpg");
           p5.setMedida("1 livro");
           p5.setPreco(25.50);
           p5.setQuantidade(15);
           produtoRepository.save(p5);

           Produto p6 = new Produto();
           p6.setNome("O Conto da Aia");
           p6.setDescricao("Autora: Margaret Atwood");
           p6.setLinkImg("./../assets/o-conto-da-aia.jpg");
           p6.setMedida("1 livro");
           p6.setPreco(40.00);
           p6.setQuantidade(25);
           produtoRepository.save(p6);

           Produto p7 = new Produto();
           p7.setNome("Olhos D'água");
           p7.setDescricao("Autora: Conceição Evaristo");
           p7.setLinkImg("./../assets/olhos-dagua.jpg");
           p7.setMedida("1 livro");
           p7.setPreco(55.00);
           p7.setQuantidade(10);
           produtoRepository.save(p7);

           Produto p8 = new Produto();
           p8.setNome("Senhor dos Anéis");
           p8.setDescricao("Autores: J. R. R. Tolkien, Ronald E. Kyrmse");
           p8.setLinkImg("./../assets/box-senhordosaneis.jpg");
           p8.setMedida("Box Trilogia");
           p8.setPreco(150.00);
           p8.setQuantidade(3);
           produtoRepository.save(p8);

           Produto p9 = new Produto();
           p9.setNome("O Espetacular Homem-Aranha: Vol.1");
           p9.setDescricao("Autor: Stan Lee");
           p9.setLinkImg("./../assets/homem-aranha.jpg");
           p9.setMedida("1 HQ");
           p9.setPreco(130.00);
           p9.setQuantidade(5);
           produtoRepository.save(p9);
        };
    }

    @Bean
    @SuppressWarnings("unchecked")
    public FilterRegistrationBean simpleCorsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Collections.singletonList("*"));
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }

    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
        loggingFilter.setIncludeClientInfo(true);
        loggingFilter.setIncludeQueryString(true);
        loggingFilter.setIncludePayload(true);
        return loggingFilter;
    }
}
