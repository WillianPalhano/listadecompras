package br.com.st.resource;

import br.com.st.domain.Produto;
import br.com.st.service.ProdutoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
class ProdutoResource {
    private ProdutoService service;

    private Logger log = LoggerFactory.getLogger(Produto.class);

    public ProdutoResource(ProdutoService service) {
        this.service = service;
    }

    @GetMapping(value = "produtos", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Produto>> findAll() {
        List<Produto> response = null;
        try {
            response = this.service.findAll();
            log.debug("produtos: {}", response);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @PostMapping(value = "produtos", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Produto> create(@RequestBody Produto produto) {
        Produto response;
    	try {
            response = this.service.save(produto);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping(value = "produtos", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Produto> save(@RequestBody Produto produto) {
        Produto response;
    	try {
            response = this.service.save(produto);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}