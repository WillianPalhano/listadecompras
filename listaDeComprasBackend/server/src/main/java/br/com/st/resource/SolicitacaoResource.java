package br.com.st.resource;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.st.domain.Solicitacao;
import br.com.st.service.SolicitacaoService;

@RestController
class SolicitacaoResource {
    private SolicitacaoService service;

    private Logger log = LoggerFactory.getLogger(Solicitacao.class);

    public SolicitacaoResource(SolicitacaoService service) {
        this.service = service;
    }

    @GetMapping(value = "solicitacoes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Solicitacao>> findAll() {
        List<Solicitacao> response = null;
        try {
            response = this.service.findAll();
            log.debug("solicitacoes: {}", response);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @PostMapping(value = "solicitacoes", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Solicitacao> save(@RequestBody Solicitacao solicitacao) {
        Solicitacao response;
    	try {
            response = this.service.save(solicitacao);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}