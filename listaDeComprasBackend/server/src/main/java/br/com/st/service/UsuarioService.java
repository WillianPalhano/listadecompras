package br.com.st.service;

import br.com.st.domain.Usuario;

import java.util.List;

public interface UsuarioService {

    List<Usuario> findAll();
    Usuario save(Usuario u);
}
