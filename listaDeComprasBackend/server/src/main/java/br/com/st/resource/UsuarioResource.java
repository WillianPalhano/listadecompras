package br.com.st.resource;

import br.com.st.domain.Usuario;
import br.com.st.service.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
class UsuarioResource {
    private UsuarioService service;

    private Logger log = LoggerFactory.getLogger(Usuario.class);

    public UsuarioResource(UsuarioService service) {
        this.service = service;
    }

    @GetMapping(value = "usuarios", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Usuario>> findAll() {
        List<Usuario> response = null;
        try {
            response = this.service.findAll();
            log.debug("usuarios: {}", response);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @PostMapping(value = "usuarios", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Usuario> save(@RequestBody Usuario usuario) {
        Usuario response;
    	try {
            response = this.service.save(usuario);
        }catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}