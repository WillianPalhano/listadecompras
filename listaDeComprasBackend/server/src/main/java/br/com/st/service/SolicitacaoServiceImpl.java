package br.com.st.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.st.domain.Solicitacao;
import br.com.st.repository.SolicitacaoRepository;

@Service
public class SolicitacaoServiceImpl implements SolicitacaoService {

    @Autowired
    private SolicitacaoRepository solicitacaoRepository;

    @Override
    public List<Solicitacao> findAll() {
        return this.solicitacaoRepository.findAll();
    }

    @Override
    public Solicitacao save(Solicitacao u) {
        return this.solicitacaoRepository.save(u);
    }
}
