package br.com.st.service;

import java.util.List;

import br.com.st.domain.Solicitacao;

public interface SolicitacaoService {

    List<Solicitacao> findAll();
    
    Solicitacao save(Solicitacao u);
}
