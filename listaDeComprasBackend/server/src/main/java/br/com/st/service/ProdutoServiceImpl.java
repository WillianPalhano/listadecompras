package br.com.st.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.st.domain.Produto;
import br.com.st.repository.ProdutoRepository;

@Service
public class ProdutoServiceImpl implements ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Override
    public List<Produto> findAll() {
        return this.produtoRepository.findAll();
    }

    @Override
    public Produto save(Produto u) {
        return this.produtoRepository.save(u);
    }

    @Override
    public Produto create(Produto u) {
        return this.produtoRepository.save(u);
    }
}
